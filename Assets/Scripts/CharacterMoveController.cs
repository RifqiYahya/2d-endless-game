using UnityEngine;

public class CharacterMoveController : MonoBehaviour
{
    [Header("Movement")]
    public float MoveAccel;
    public float MaxSpeed;

    Rigidbody2D rig;

    [Header("Jump")]
    public float JumpAccel;
    public GameObject TutorialText;

    bool isJumping;
    bool isOnGround;

    [Header("Ground Raycast")]
    public float GroundRaycastDistance;
    public LayerMask GroundLayerMask;

    Animator animator;
    CharacterSoundController sound;
    
    [Header("Scoring")]
    public ScoreController score;
    public float ScoringRatio;
    float lastPositionX;

    [Header("Game Over")]
    public GameObject GameOverScreen;
    public float FallPositionY;

    [Header("Camera")]
    public CameraMoveController GameCamera;

    
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sound = GetComponent<CharacterSoundController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if(isOnGround) {
                isJumping = true;
                sound.PlayJump();
                TutorialText.SetActive(false);
            }
        }

        animator.SetBool("isOnGround", isOnGround);
        
        int distancePassed = Mathf.FloorToInt(transform.position.x - lastPositionX);
        int scoreIncrement = Mathf.FloorToInt(distancePassed/ScoringRatio);

        if (scoreIncrement > 0) {
            score.IncreaseCurrentScore(scoreIncrement);
            lastPositionX += distancePassed;
        }

        if (transform.position.y < FallPositionY) {
            GameOver();
        }
    }

    void FixedUpdate() {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, GroundRaycastDistance, GroundLayerMask);

        if (hit) {
            if(!isOnGround && rig.velocity.y <= 0) {
                isOnGround = true;
            }
        } else {
            isOnGround = false;
        }

        Vector2 velocityVector = rig.velocity;

        if (isJumping) {
            velocityVector.y += JumpAccel;
            isJumping = false;
        }

        velocityVector.x = Mathf.Clamp(velocityVector.x + MoveAccel*Time.fixedDeltaTime, 0f, MaxSpeed);
        rig.velocity = velocityVector;
    }

    void OnDrawGizmos() {
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * GroundRaycastDistance), Color.white);
    }

    void GameOver() {
        score.FinishScoring();
        GameCamera.enabled = false;
        this.enabled = false;
        GameOverScreen.SetActive(true);
    }

}
