using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScoreController : MonoBehaviour
{
    [Header("UI")]
    public Text Score;
    public Text HighScore;

    [Header("Score")]
    public ScoreController scoreController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Score.text = scoreController.GetCurrentScore().ToString("0");
        HighScore.text = ScoreData.highScore.ToString("0");
    }
}
