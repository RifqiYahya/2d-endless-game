using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerrainGeneratorController : MonoBehaviour
{
    [Header("Templates")]
    public List<TerrainTemplateController> TerrainTemplates;
    public float terrainTemplateWidth;

    [Header("Generator Area")]
    public Camera GameCamera;
    public float AreaStartOffset;
    public float AreaEndOffset;

    const float debugLineHeight = 10.0f;

    private List<GameObject> spawnedTerrain;

    float lastGeneratedPositionX;
    float lastRemovedPositionX;

    [Header("Force Early Template")]
    public List<TerrainTemplateController> earlyTerrainTemplates;

    private Dictionary<string, List<GameObject>> pool;

    // [Header("Debug text")]
    // public Text DebugText;

    float GetHorizontalPointStart() {
        return GameCamera.ViewportToWorldPoint(new Vector2(0f, 0f)).x + AreaStartOffset;
    }

    float GetHorizontalPointEnd() {
        return GameCamera.ViewportToWorldPoint(new Vector2(1f, 0f)).x + AreaEndOffset;
    }

    void OnDrawGizmos() {
        Vector3 areaStartPosition = transform.position;
        Vector3 areaEndPosition = transform.position;

        areaStartPosition.x = GetHorizontalPointStart();
        areaEndPosition.x = GetHorizontalPointEnd();

        Debug.DrawLine(areaStartPosition + Vector3.up*debugLineHeight/2, areaStartPosition + Vector3.down*debugLineHeight/2, Color.red);
        Debug.DrawLine(areaEndPosition + Vector3.up*debugLineHeight/2, areaEndPosition + Vector3.down*debugLineHeight/2, Color.red);
        // Vector3 test = new Vector3(lastRemovedPositionX ,transform.position.y, transform.position.z);
        // Debug.DrawLine(test + Vector3.up*debugLineHeight/2, test + Vector3.down*debugLineHeight/2, Color.white);
    }

    // Start is called before the first frame update
    void Start()
    {
        pool = new Dictionary<string, List<GameObject>>();

        spawnedTerrain = new List<GameObject>();
        lastGeneratedPositionX = GetHorizontalPointStart();
        lastRemovedPositionX = lastGeneratedPositionX - terrainTemplateWidth;

        foreach(TerrainTemplateController terrain in earlyTerrainTemplates) {
            GenerateTerrain(lastGeneratedPositionX, terrain);
            lastGeneratedPositionX += terrainTemplateWidth;
        }

        while (lastGeneratedPositionX < GetHorizontalPointEnd()) {
            GenerateTerrain(lastGeneratedPositionX);
            lastGeneratedPositionX += terrainTemplateWidth;
        }
    }

    // Update is called once per frame
    void Update()
    {
        while (lastGeneratedPositionX < GetHorizontalPointEnd()) {
            GenerateTerrain(lastGeneratedPositionX);
            lastGeneratedPositionX += terrainTemplateWidth;
        }

        while (lastRemovedPositionX + terrainTemplateWidth < GetHorizontalPointStart()) {
            lastRemovedPositionX += terrainTemplateWidth;
            RemoveTerrain(lastGeneratedPositionX);
        }

        // DebugText.text = $"Log : \nPool length : {pool.Count}\n";

        // foreach(KeyValuePair<string, List<GameObject>> entry in pool) {
        //     DebugText.text = DebugText.text + $"{entry.Key} \nLength : {entry.Value.Count}\n";
        // }
    }

    GameObject GenerateFromPool(GameObject item, Transform parent) {
        if (pool.ContainsKey(item.name)) {
            if(pool[item.name].Count > 0) {
                GameObject newItemFromPool = pool[item.name][0];
                pool[item.name].Remove(newItemFromPool);
                return newItemFromPool;
            }
        } else {
            pool.Add(item.name, new List<GameObject>());
        }

        GameObject newItem = Instantiate(item, parent);
        newItem.name = item.name;
        return newItem;
    }

    void ReturnToPool(GameObject item) {
        if (!pool.ContainsKey(item.name)) {
            Debug.LogError("Invalid pool item");
        }

        pool[item.name].Add(item);
        item.SetActive(false);
    }

    void GenerateTerrain(float posX, TerrainTemplateController forceterrain = null) {

        GameObject newTerrain = forceterrain ? GenerateFromPool(forceterrain.gameObject, transform) : 
            GenerateFromPool(TerrainTemplates[Random.Range(0, TerrainTemplates.Count)].gameObject, transform);
        newTerrain.transform.position = new Vector2(posX, 0f);
        newTerrain.SetActive(true);
        spawnedTerrain.Add(newTerrain);
    }

    void RemoveTerrain(float posX) {
        GameObject terrainToRemove = null;

        foreach(GameObject item in spawnedTerrain) {
            if (item.transform.position.x <= posX) {
                terrainToRemove = item;
                break;
            }
        }

        if (terrainToRemove != null) {
            spawnedTerrain.Remove(terrainToRemove);
            ReturnToPool(terrainToRemove);
        }
    }
}
