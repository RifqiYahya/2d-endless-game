using UnityEngine;

public class ScoreController : MonoBehaviour
{
    private int currentScore = 0;

    [Header("Score Highlight")]
    public int ScoreHighlightRange;
    public CharacterSoundController Sound;

    int lastScoreHiglight;

    // Start is called before the first frame update
    void Start()
    {
        currentScore = 0;
        lastScoreHiglight = 0;
    }

    public float GetCurrentScore() {
        return currentScore;
    }

    public void IncreaseCurrentScore(int increment) {
        currentScore += increment;

        if (currentScore - lastScoreHiglight > ScoreHighlightRange){
            Sound.PlayScoreHighlight();
            lastScoreHiglight += ScoreHighlightRange;
        }
    }

    public void FinishScoring() {
        if (currentScore > ScoreData.highScore) {
            ScoreData.highScore = currentScore;
        }
    }
}
