using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSoundController : MonoBehaviour
{
    public AudioClip Jump;
    public AudioClip ScoreHighlight;
    private AudioSource audioPlayer;

    // Start is called before the first frame update
    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
    }

    public void PlayScoreHighlight() {
        audioPlayer.PlayOneShot(ScoreHighlight);
    }

    public void PlayJump() {
        audioPlayer.PlayOneShot(Jump);
    }
}
