using UnityEngine;

public class TerrainTemplateController : MonoBehaviour
{
    const float debugLineHeight = 10.0f;

    void OnDrawGizmos() {
        Debug.DrawLine(transform.position + Vector3.up*debugLineHeight/2, transform.position + Vector3.down*debugLineHeight/2, Color.green);
    }
}
